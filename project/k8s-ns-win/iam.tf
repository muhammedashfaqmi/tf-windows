resource "aws_iam_role" "this-write" {
  count = var.create && var.create_default_roles ? 1 : 0

  name = "${local.name_full}-write"
  path = "/${local.iam_role_prefix}/"

  permissions_boundary = local.permission_boundary
  assume_role_policy   = data.aws_iam_policy_document.this-assume[0].json

  tags = local.tags
}

resource "aws_iam_role" "this-read" {
  count = var.create && var.create_default_roles ? 1 : 0

  name = "${local.name_full}-read"
  path = "/${local.iam_role_prefix}/"

  permissions_boundary = local.permission_boundary
  assume_role_policy   = data.aws_iam_policy_document.this-assume[0].json

  tags = local.tags
}

data "aws_iam_policy_document" "this-assume" {
  count = var.create && var.create_default_roles ? 1 : 0

  version = "2012-10-17"

  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = concat(local.env.iam_eks_admin_role != null ? [local.env.iam_eks_admin_role] : [], [
        local.k8s_iam_nodes_role_arn,
      ])
    }
  }

  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.${data.aws_partition.current.dns_suffix}"]
    }
  }
}

