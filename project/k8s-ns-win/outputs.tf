output "service" {
  value = var.service

  description = "Service name grouping set of modules"
}

output "name" {
  value = var.name

  description = "Shared name of this module"
}

output "tags" {
  value = var.tags

  description = "Map of tags for nested module resources"
}

output "iam_role_prefix" {
  value = local.iam_role_prefix
}

output "k8s_pod_tolerations" {
  value = local.k8s_pod_tolerations
}

output "k8s_pod_node_selector" {
  value = local.k8s_pod_node_selector
}

output "k8s_node_taints" {
  value = local.k8s_node_taints
}

output "k8s_node_labels" {
  value = local.k8s_node_labels
}

output "k8s_namespace" {
  value = local.ns_name
}
