locals {
  create_resources = var.create ? 1 : 0

  # Service name grouping set of modules
  service = var.service != null ? var.service : var.name

  # Shared name of this module. Should be used as a prefix for submodules
  name = var.service != null ? "${var.service}-${var.name}" : var.name

  # Full module name with all prefixes (to be used as resource names)
  name_full = "${local.module_prefix}${var.project != null ? "${var.project}-" : ""}${local.name}"

  # Map of tags that should be used by submodules
  tags_common = merge(var.tags, {
    format(local.shared_tag, "extras/terraform-module/${var.module_name}/version") = var.module_version
  })

  # Map of tags used for resources created by this module
  tags = merge(local.tags_common, {
    format(local.shared_tag, "terraform-module") = var.module_name
    format(local.shared_tag, "service")          = local.service
    format(local.shared_tag, "role")             = "K8s namespace"
  })

  # For resources with limited amount of tags allowed (e.g. S3 objects)
  tags_short = {for k, v in local.tags : k => v if replace(k, format(local.shared_tag, "extras/"), "") == k}
}

locals {
  ns_role = var.project != null ? "project" : "system"
  ns_name = var.project != null ? "project-${var.project}${var.release != null ? "-${var.release}" : ""}" : "${var.name}-system"

  iam_role_prefix = local.name_full

  ns_annotations = {
    // Not used as EKS doesn't support legacy admission controllers
    "scheduler.alpha.kubernetes.io/defaultTolerations" = "[]"
    "scheduler.alpha.kubernetes.io/node-selector"      = ""

    "iam.amazonaws.com/allowed-roles" = jsonencode(concat(var.create && var.create_default_roles ? [
      aws_iam_role.this-read[0].arn,
      aws_iam_role.this-write[0].arn,
    ]:[], [
      var.project != null ? "${local.iam_role_prefix}/.*" : ".*"
    ]))
  }
  ns_labels      = merge(var.istio_enabled? {
    istio-injection = "enabled"
  }: {}, {})

  data_dir = "${path.module}/data"

  ansible_directory = "${path.root}/${var.ansible_directory_prefix}${local.name}"

  ansible_tags = concat(var.create_default_roles ? [
    "accounts"] : [], [])

  k8s_taints_map = merge(map(
  replace(format(local.shared_tag, "role"), "https://", ""),
  local.ns_role,
  replace(format(local.shared_tag, "service"), "https://", ""),
  local.service,
  replace(format(local.shared_tag, "name"), "https://", ""),
  local.name,
  replace(format(local.shared_tag, "environment"), "https://", ""),
  var.environment,
  ), var.project != null ? map(
  replace(format(local.shared_tag, "project"), "https://", ""),
  var.project
  ) : {}, var.release != null ? map(
  replace(format(local.shared_tag, "release"), "https://", ""),
  var.release
  ) : {})

  k8s_pod_tolerations = [for k, v in local.k8s_taints_map : {
    key      = k
    operator = "Equal"
    value    = v
    effect   = "NoSchedule"
  }]

  k8s_node_taints = [for k, v in local.k8s_taints_map : {
    key    = k
    value  = v
    effect = "NoSchedule"
  }]
  k8s_node_labels = {for k, v in local.k8s_taints_map : k => v}

  k8s_pod_node_selector = local.k8s_taints_map

  labels = local.k8s_node_labels
}

