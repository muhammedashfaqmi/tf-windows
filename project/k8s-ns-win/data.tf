data "terraform_remote_state" "base" {
  count = local.create_resources * (var.use_remote_state ? 1 : 0)

  backend = "s3"
  config  = {
    bucket  = "hilti-infra-${var.environment}"
    key     = "terraform/base.tfstate"
    region  = data.aws_region.current.name
    profile = "default"
  }
}
data "aws_partition" "current" {}

data "aws_region" "current" {}

data "aws_eks_cluster" "target" {
  count = local.create_resources

  name = local.k8s_cluster_name
}
