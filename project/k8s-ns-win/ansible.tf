module "ansible" {
  source = "https://s3-eu-west-1.amazonaws.com/hilti-infra-public/shared-artifacts/terraform-modules/ansible/0.1.0-master.zip"
  create = true

  project     = null
  service     = "infra"
  name        = local.name
  environment = var.environment
  tags        = local.tags

  use_remote_state = false

  module_prefix = local.module_prefix
  shared_tag    = local.shared_tag

  ansible_dir_source     = local.data_dir
  ansible_dir_target     = local.ansible_directory
  ansible_tags           = local.ansible_tags
  ansible_variables_json = local.ansible_variables_json
}

locals {
  ansible_variables_json = var.create ? jsonencode({
    common = {
      labels    = local.labels
      namespace = local.ns_name
    }

    accounts = {
      writer = "namespace-writer"
      reader = "namespace-reader"
    }

    namespace = {
      annotations = local.ns_annotations
      labels      = local.ns_labels
    }
  }) : "{}"
}
