variable "create" {
  type        = bool
  description = "If we want to create module resources"

  default = true
}

variable "module_version" {
  type        = string
  description = "Version of the current module"

  default = "0.1.0-master"
}

variable "module_name" {
  type        = string
  description = "Name of the current module"

  default = "k8s-ns"
}

variable "project" {
  type        = string
  description = "Project name"

  default = null
}

variable "service" {
  type        = string
  description = "Service name. \"name\" field is used if omitted"

  default = null
}

variable "name" {
  type        = string
  description = "Resource name. If service parameter is used, it is added as a prefix"
}

variable "environment" {
  type        = string
  description = "Environment name"
}

variable "tags" {
  type        = map(string)
  description = "Map of common tags for all resources"

  default = {}
}

variable "ansible_directory_prefix" {
  type    = string
  default = "ansible/"
}

variable "use_remote_state" {
  type        = bool
  description = "If we need to use remote terraform-infra-base state"

  default = true
}

variable "release" {
  type        = string
  description = "Release name"

  default = null
}

variable "istio_enabled" {
  type    = bool
  default = true
}

variable "create_default_roles" {
  type    = bool
  default = true
}
