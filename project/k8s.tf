module "k8s-ns" {
  source = "./k8s-ns-win"
  project     = var.name
  service     = "k8s"
  name        = "project"
  environment = var.environment
  release     = var.release

  ansible_directory_prefix = "artifacts/ansible/k8s-ns/00-"

  tags = local.tags

 providers = {
    aws         = aws
    aws.route53 = aws.route53
  }
}


module "k8s-ng" {
  source = "./k8s-ng-win"
  project     = var.name
  service     = "k8s"
  name        = "project"
  environment = var.environment

  tags = local.tags

  ec2_instance_type    = "t3.medium"
  ec2_sg_min           = 0
  ec2_sg_max           = 3
  ec2_root_volume_size = 50

  k8s_node_labels = module.k8s-ns.k8s_node_labels
  k8s_node_taints = module.k8s-ns.k8s_node_taints
}
