resource "aws_cloudwatch_log_group" "this" {
  count = var.create && var.logs_enable ? 1 : 0

  name              = "${local.module_prefix}${var.project != null ? "${var.project}/" : ""}${local.service}/${local.name}"
  retention_in_days = var.logs_retention_days

  tags = local.tags
}
