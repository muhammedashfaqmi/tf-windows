resource "aws_iam_instance_profile" "this" {
  count = local.create_resources

  name = local.name_full
  role = local.role_id
}

resource "aws_iam_role" "this" {
  count = local.create_resources * (var.iam_role_name_override ? 1 : 0)

  name = local.name_full
  path = "/"

  permissions_boundary = local.permission_boundary
  assume_role_policy   = data.aws_iam_policy_document.ec2-assume[0].json

  tags = local.tags
}

resource "aws_iam_role_policy_attachment" "ssm-access" {
  count = local.create_resources * (var.iam_enable_ssm_access ? 1 : 0) * (var.iam_role_name_override ? 1 : 0)

  role       = aws_iam_role.this[0].name
  policy_arn = data.aws_iam_policy.ssm-access[0].arn
}

resource "aws_iam_policy" "cloudwatch-access" {
  count =  local.create_resources * (var.logs_enable ? 1 : 0)

  name        = "${local.name_full}-cloudwatch-access"
  description = "Allows ${local.name_full} lambda writing logs and metrics"

  policy = data.aws_iam_policy_document.cloudwatch-access[0].json
}

resource "aws_iam_role_policy_attachment" "cloudwatch-access" {
  count = local.create_resources * (var.logs_enable ? 1 : 0)

  role       = local.role_id
  policy_arn = aws_iam_policy.cloudwatch-access[0].arn
}
