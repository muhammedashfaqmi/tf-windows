output "service" {
  value = local.service

  description = "Service name grouping set of modules"
}

output "name" {
  value = local.name

  description = "Shared name of this module"
}

output "name_full" {
  value = local.name_full

  description = "Full module name with all prefixes"
}

output "tags" {
  value = local.tags

  description = "Map of tags for nested module resources"
}

output "tags_common" {
  value = local.tags_common

  description = "Map of common module tags"
}

output "cloudwatch_log_group" {
  value = var.create && var.logs_enable ? aws_cloudwatch_log_group.this.*.name : null

  description = "Log group name"
}

output "iam_role_name" {
  value = var.create ? local.role_id : null

  description = "EC2 instance profile role name"
}

output "iam_role_arn" {
  value = var.create ? (var.iam_role_name != null ? data.aws_iam_role.overriden_role[0].arn : aws_iam_role.this[0].arn) : null

  description = "EC2 instance profile role arn"
}

output "ec2_security_group_id" {
  value = local.sg_id

  description = "EC2 security group id"
}

output "ec2_autoscaling_group_id" {
  value = var.create ? aws_autoscaling_group.this[0].id : null

  description = "EC2 instance group id"
}
