variable "name" {
  type        = string
  description = "Resource name. If service parameter is used, it is added as a prefix"
}

variable "ec2_instance_type" {
  type        = string
  description = "EC2 instance type"
}

variable "ec2_count" {
  type        = number
  description = "EC2 instance count"

  default = 1
}

variable "ec2_root_volume_size" {
  type        = number
  description = "EBS root volume size in gigabytes"

  default = 20
}

variable "vpc_subnet_id" {
  type        = string
  description = "VPC Subnet id for instance. First subnet is used if omitted"

  default = null
}
