#!/usr/bin/env bash

EC2_USER="ec2-user"
INSTANCE_ID="$(wget -q -O - http://169.254.169.254/latest/meta-data/instance-id)"
%{ if docker_install }
DOCKER_DATA_ROOT="/var/lib/docker"
%{ endif }


echo "Setting up ssh keys:"
ssh_keys=(%{ for key in ssh_keys }"${key}" %{ endfor })
setup_ssh_keys "$${EC2_USER}" "$${ssh_keys[@]}"


echo "Initial setup:"
setup_initial


if [ "$${DATA_VOLUME_ID}" != "" ]; then
  echo "Attaching volume $${DATA_VOLUME_ID}:"
  volume_attach "$${DATA_VOLUME_ID}" "$${INSTANCE_ID}" "$${DATA_DEVICE}"

  echo "Mounting data volume $${DATA_DEVICE} to $${DATA_MOUNT}"
  volume_format_and_mount "$${DATA_DEVICE}" "$${DATA_MOUNT}"

  %{ if docker_install }
  DOCKER_DATA_ROOT="$${DATA_MOUNT}/docker"
  %{ endif }
fi



%{ if docker_install }
echo "Installing docker:"
setup_docker "$${EC2_USER}" "$${DOCKER_DATA_ROOT}" "${cloudwatch_log_group}"


%{ if docker_install_cleanup }
echo "Installing docker cleanup image:"
setup_docker_cleanup "cleanup"
%{ endif }
%{ endif }
