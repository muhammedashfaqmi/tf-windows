#!/usr/bin/env bash

function volume_get_status() {
  aws ec2 describe-volumes \
    --region "${aws_region}" \
    --volume-ids "$${1}" \
    --query "Volumes[*].Attachments[*].State" \
    --output text
}

function volume_wait() {
  until [ "$(volume_get_status "$${1}")" == "attached" ]
  do
    sleep 1
  done
  sleep 1
}

function volume_attach() {
  aws ec2 attach-volume \
    --region "${aws_region}" \
    --volume-id "$${1}" \
    --instance-id "$${2}" \
    --device "$${3}" || true

  volume_wait "$${1}"
}

function volume_format_and_mount() {
  FORMAT_CMD="mkfs -t ext4"
  if [ "$${3}" != "" ]; then
    FORMAT_CMD="mkfs -t ext4 -N $${3}"
  fi

  if [ "$(file -b -s -L "$${1}")" == "data" ]; then
    $${FORMAT_CMD} "$${1}"
  fi

  mkdir -p "$${2}"
  mount "$${1}" "$${2}"
}

function setup_initial() {
  mkdir -p /var/lib/rpm-state
  yum update -y
}

function setup_docker() {
  yum install -y docker
  usermod -a -G docker "$${1}" || true
  service docker stop || true

  if [ "$${3}" != "" ]; then
    cat > /etc/docker/daemon.json <<EOF
{
  "storage-driver": "overlay2",
  "data-root": "$${2}",
  "log-driver": "awslogs",
  "log-opts": {
    "tag": "docker.{{.Name}}.{{.ID}}",
    "awslogs-region": "${aws_region}",
    "awslogs-group": "$${3}"
  }
}
EOF
  fi
  service docker start
}

function setup_docker_cleanup() {
  docker stop "$${1}" || true
  docker rm "$${1}" || true
  docker run -d \
    --name "$${1}" \
    -v /var/run/docker.sock:/var/run/docker.sock:rw \
    -v /var/lib/docker:/var/lib/docker:rw \
    --restart unless-stopped \
  meltwater/docker-cleanup:latest
}

function setup_ssh_keys_add() {
  KEYS_PATH="/home/$${1}/.ssh/authorized_keys"
  echo "$${2}" >> "$${KEYS_PATH}"
}

function setup_ssh_keys() {
  mkdir -p "/home/$${1}/.ssh"
  chmod 700 "/home/$${1}/.ssh"
  KEYS_PATH="/home/$${1}/.ssh/authorized_keys"
  truncate -s 0 "$${KEYS_PATH}"
  chmod 600 "$${KEYS_PATH}"
  chown -Rv "$${1}:$${1}" "/home/$${1}/.ssh"

  ssh_keys=("$${@:2}")
  for key in "$${ssh_keys[@]}"; do
    setup_ssh_keys_add "$${1}" "$${key}"
  done
}
