data "terraform_remote_state" "base" {
  count = var.use_remote_state ? 1 : 0

  backend = "s3"
  config  = {
    bucket  = "hilti-infra-${var.environment}"
    key     = "terraform/base.tfstate"
    region  = data.aws_region.current.name
    profile = "default"
  }
}
data "aws_partition" "current" {}

data "aws_iam_role" "overriden_role" {
  count = var.create && var.iam_role_name_override ? 1 : 0

  name = var.iam_role_name
}

# https://github.com/ukayani/cloud-init-example/blob/master/README.md
# https://aws.amazon.com/premiumsupport/knowledge-center/execute-user-data-ec2/
data "template_cloudinit_config" "this" {
  count = local.create_resources

  gzip          = false
  base64_encode = true

  dynamic "part" {
    for_each = concat(
    local.cloudconfig_parts_alwaysrun,
    local.cloudconfig_parts_init,
    local.cloudconfig_parts_additional_startup,
    var.cloudconfig_parts
    )
    content {
      filename     = lookup(part.value, "filename", null)
      content      = lookup(part.value, "content", null)
      content_type = lookup(part.value, "content_type", null)
      merge_type   = lookup(part.value, "merge_type ", null)
    }
  }
}

data "aws_iam_policy_document" "ec2-assume" {
  count = local.create_resources

  version = "2012-10-17"

  statement {
    effect  = "Allow"
    principals {
      identifiers = ["ec2.${data.aws_partition.current.dns_suffix}"]
      type        = "Service"
    }
    actions = ["sts:AssumeRole"]
  }
}

data "aws_region" "current" {}

data "aws_iam_policy" "ssm-access" {
  count = local.create_resources * (var.iam_enable_ssm_access ? 1 : 0) * (var.iam_role_name_override ? 1 : 0)

  # https://docs.aws.amazon.com/systems-manager/latest/userguide/setup-instance-profile.html
  arn = "arn:${data.aws_partition.current.partition}:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

data "aws_iam_policy_document" "cloudwatch-access" {
  count = local.create_resources * (var.logs_enable ? 1 : 0)

  version = "2012-10-17"

  statement {
    effect    = "Allow"
    resources = [
      "arn:${data.aws_partition.current.partition}:logs:${local.aws_region}:${local.env.account_id}:log-group:*:log-stream:*",
    ]
    actions   = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
      "logs:GetLogEvents",
    ]
  }

  statement {
    effect    = "Allow"
    resources = ["*"] // TODO: instance IDs from SG?
    actions   = [
      "ec2:DescribeTags",
    ]
  }

  statement {
    effect    = "Allow"
    resources = [
      "*",
    ]
    actions   = [
      "cloudwatch:PutMetricData",
    ]
  }
}
