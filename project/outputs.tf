output "k8s_pod_node_selector" {
  value = module.k8s-ns.k8s_pod_node_selector
}

output "k8s_pod_tolerations" {
  value = module.k8s-ns.k8s_pod_tolerations
}

output "k8s_namespace" {
  value = module.k8s-ns.k8s_namespace
}
