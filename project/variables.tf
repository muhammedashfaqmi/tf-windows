variable "name" {
  type        = string
  description = "Project name"
}

variable "environment" {
  type        = string
  description = "Environment name"
}

variable "tags" {
  type = map(string)
  description = "Project-wide tags"
}

variable "release" {
  type = string
}

locals {
  tags = var.tags
}
