locals {
  # Service name grouping set of modules
  service = var.service != null ? var.service : var.name

  # Shared name of this module. Should be used as a prefix for submodules
  name = var.service != null ? "${var.service}-${var.name}" : var.name

  k8s_owned_map = merge({
    format("kubernetes.io/cluster/%s", local.k8s_cluster_name == null ? "" : local.k8s_cluster_name)     = "owned"
    format("k8s.io/cluster-autoscaler/%s", local.k8s_cluster_name == null ? "" : local.k8s_cluster_name) = "true"
    "k8s.io/cluster-autoscaler/enabled"                            = "true"
  },
  {for k, v in var.k8s_node_labels : format("k8s.io/cluster-autoscaler/node-template/label/%s", k) => v},
  {for taint in var.k8s_node_taints : format("k8s.io/cluster-autoscaler/node-template/taint/%s", taint.key) => "${taint.value}:${taint.effect}"}
  )

  system_labels = [for k, v in var.k8s_node_labels : "${k}=${v}"]
  system_taints = [for taint in var.k8s_node_taints : "${taint.key}=${taint.value}:${taint.effect}"]


  system_args = [
    "--node-labels=${join(",", local.system_labels)}",
    "--register-with-taints=${join(",", local.system_taints)}",
    "--runtime-request-timeout=30m",
  ]
}
