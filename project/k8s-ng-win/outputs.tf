output "service" {
  value = var.service

  description = "Service name grouping set of modules"
}

output "name" {
  value = var.name

  description = "Shared name of this module"
}

output "tags" {
  value = var.tags

  description = "Map of tags for nested module resources"
}

output "cloudwatch_log_group" {
  value = module.ig.cloudwatch_log_group

  description = "Log group name"
}

output "iam_role_name" {
  value = module.ig.iam_role_name

  description = "EC2 instance profile role name"
}

output "iam_role_arn" {
  value = module.ig.iam_role_arn

  description = "EC2 instance profile role arn"
}

output "ec2_security_group_id" {
  value = module.ig.ec2_security_group_id

  description = "EC2 security group id"
}

output "ec2_autoscaling_group_id" {
  value = module.ig.ec2_autoscaling_group_id

  description = "EC2 autoscaling group id"
}
